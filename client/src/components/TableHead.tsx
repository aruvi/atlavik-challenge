import React from "react"
import TableCell from "@material-ui/core/TableCell"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import TableSortLabel from "@material-ui/core/TableSortLabel"

const headCells: HeadCell[] = [
  { id: "rank", numeric: false, disablePadding: true, label: "RANK" },
  { id: "name", numeric: true, disablePadding: false, label: "NAME [SYMBOL]" },
  { id: "price", numeric: true, disablePadding: false, label: "PRICE (₹)" },
  { id: "market_cap", numeric: true, disablePadding: false, label: "MARKET CAP (₹)" },
  { id: "volume_24h", numeric: true, disablePadding: false, label: "VOLUME 24H" },
  { id: "delta_24h", numeric: true, disablePadding: false, label: "DELTA 24H" }
]

export default function EnhancedTableHead(props: EnhancedTableProps) {
  const { order, orderBy, onRequestSort } = props

  const createSortHandler = (property: keyof coinData) => (event: React.MouseEvent<unknown>) => {
    onRequestSort(event, property)
  }

  return (
    <TableHead>
      <TableRow>
        {
          headCells.map((headCell) => (
            <TableCell
              key={headCell.id as string}
              align={"center"}
              padding={headCell.disablePadding ? "default" : "default"}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : "asc"}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
              </TableSortLabel>
            </TableCell>
          ))}
      </TableRow>
    </TableHead>
  )
}
