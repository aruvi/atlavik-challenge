import React, { useEffect } from "react"
import { useDebouncedValue } from "./debouncedValue"
import { createStyles, lighten, makeStyles, Theme } from "@material-ui/core/styles"
import Toolbar from "@material-ui/core/Toolbar"
import Typography from "@material-ui/core/Typography"
import Paper from "@material-ui/core/Paper"
import InputBase from "@material-ui/core/InputBase"
import CircularProgress from "@material-ui/core/CircularProgress"

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      width: "20%",
      height: "40px",
      display: "flex",
      position: "absolute",
      right: 35
    },
    searchInput: {
      width: "100%"
    },
    highlight:
      theme.palette.type === "light"
        ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
        : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
    title: {
      flex: "1 1 100%",
    },
    spinner: {
      width: "25px !important",
      height: "25px !important",
      position: "absolute",
      right: 10,
      top: 7
    }
  }),
)

export default (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles()
  const [searchString, setSearchString] = React.useState<string>("")

  const handleSearchStringChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchString(event.target.value)
  }

  const debouncedValue = useDebouncedValue(searchString, 500)

  useEffect(() => {
    props.searchCallback(debouncedValue)
  }, [debouncedValue])

  return (
    <Toolbar>
      <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
        Crypto Coins List
      </Typography>
      <Paper component="form" className={classes.root}>
        <InputBase
          className={classes.searchInput}
          placeholder="Search by name"
          onChange={handleSearchStringChange}
          inputProps={{ "aria-label": "Search" }}
        />
        {props.isLoading && <CircularProgress className={classes.spinner} />}
      </Paper>
    </Toolbar>
  )
}
