import React, { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { createStyles, lighten, makeStyles, Theme } from "@material-ui/core/styles"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableContainer from "@material-ui/core/TableContainer"
import TablePagination from "@material-ui/core/TablePagination"
import TableRow from "@material-ui/core/TableRow"
import Paper from "@material-ui/core/Paper"
import Avatar from "@material-ui/core/Avatar"
import TableToolbar from "./tableToolBar"
import TableHead from "./TableHead"
import { sortCoinListSelector, coinListSelector } from "../selectors/sort-coin-list"
import { fetchCoinsList } from "../actions/coin-list-actions"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    paper: {
      width: "100%",
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
    },
    coinNameContainer: {
      display: "flex",
      alignItems: "center",
      fontSize: 18
    },
    coinIcon: {
      marginRight: 20
    }
  }),
)

export default function EnhancedTable() {
  const classes = useStyles()
  const dispatch = useDispatch()
  const sortCoinList = useSelector(sortCoinListSelector)
  const coinListState = useSelector(coinListSelector)
  const [order, setOrder] = React.useState<Order>("asc")
  const [orderBy, setOrderBy] = React.useState<keyof coinData>("rank")
  const [page, setPage] = React.useState(0)
  const [rowsPerPage, setRowsPerPage] = React.useState(25)
  let coinList = sortCoinList({ sortType: order, sortKey: orderBy })

  useEffect(() => {
    coinList = sortCoinList({ sortType: order, sortKey: orderBy })
  }, [order, orderBy, sortCoinList])

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof coinData) => {
    const isAsc = orderBy === property && order === "asc"
    setOrder(isAsc ? "desc" : "asc")
    setOrderBy(property)
  }

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const search = (searchString: string) => {
    setPage(0)
    dispatch(fetchCoinsList(searchString))
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableToolbar searchCallback={search} isLoading={coinListState.isLoading} />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={"medium"}
            aria-label="enhanced table"
          >
            <TableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={coinList.length}
            />
            <TableBody>
              {coinList
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.name}
                    >
                      <TableCell align="center">{row.rank}</TableCell>
                      <TableCell align="center">
                        <div className={classes.coinNameContainer}>
                          <div className={classes.coinIcon}>
                            <Avatar alt={row.show_symbol} src={require(`../assets/coin-icons/${row.symbol.toLowerCase()}.png`)} />
                          </div>
                          <div>
                            <span>{row.name} [{row.show_symbol}]</span>
                          </div>
                        </div>
                      </TableCell>
                      <TableCell align="center">₹{parseFloat(row.price).toFixed(2)}</TableCell>
                      <TableCell align="center">₹{parseFloat(row.market_cap).toFixed(2)}</TableCell>
                      <TableCell align="center">₹{parseFloat(row.volume_24h).toFixed(2)}</TableCell>
                      <TableCell align="center">{parseFloat(row.delta_24h).toFixed(2)}</TableCell>
                    </TableRow>
                  )
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={coinList.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  )
}
