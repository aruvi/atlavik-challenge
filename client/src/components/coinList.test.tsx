// Component.test.js
import React from "react"
import Enzyme, { mount, shallow } from "enzyme"
import EnzymeAdapter from "enzyme-adapter-react-16"
import { Provider } from "react-redux"
import configureMockStore from "redux-mock-store"
import thunk from "redux-thunk"
import { createStore } from "redux"
import CoinList from "./coinsList"
import reducer from "../reducers"
import { createMount, createShallow } from "@material-ui/core/test-utils"
import { ThemeProvider } from "@material-ui/core/styles"


const mockStore = configureMockStore([thunk])

describe("<CoinList /> unit test", () => {

  let mount
  const store = mockStore({
    coinList: {
      data: [], isLoading: false, error: {}
    }
  })

  const MockTheme = ({ children }: any) => (
    <Provider store={store}>
      <ThemeProvider theme={{ success: { main: "#fff" }, spacing: jest.fn() }}>
        {children}
      </ThemeProvider>
    </Provider>
  )

  beforeEach(() => {
    mount = createMount()
  })

  afterEach(() => {
    mount.cleanUp()
  })

  it("should add to count and display the correct # of counts", () => {
    // const wrapper = mount(<MockTheme><CoinList /></MockTheme>)
    // expect(wrapper.html()).toMatchSnapshot()
    expect(true).toBe(true)
  })
})

