import React from "react"
import { shallow } from "enzyme"
import TableHead from "./TableHead"

describe("App component", () => {
  it("should match the snapshot", () => {
    const wrapper = shallow(
      <TableHead rowCount={1} order={"asc"} orderBy={"name"} onRequestSort={jest.fn()} />
    )
    expect(wrapper.html()).toMatchSnapshot()
  })
})

