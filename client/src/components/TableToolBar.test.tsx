import React from "react"
import { shallow } from "enzyme"
import TableToolBar from "./tableToolBar"

describe("App component", () => {
  it("should match the snapshot", () => {
    const wrapper = shallow(
      <TableToolBar isLoading={false} searchCallback={jest.fn()} />
    )
    expect(wrapper.html()).toMatchSnapshot()
  })
})

