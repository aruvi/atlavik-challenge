export const fetchCoinsList = (searchString: string) => async (dispatch) => {
  dispatch(fetchStart());
  try {
    const BASE_URL = process.env.BASE_URL || "localhost:5000"
    const response = await fetch(`http://${BASE_URL}/getCoins${searchString ? '?search=' + searchString : ''}`);
    const data = await response.json();
    dispatch(fetchSuccess(data.coins));
  } catch (error) {
    dispatch(fetchFailure(error));
  }
};

export const fetchStart = () => {
  return {
    type: "FETCH-START"
  }
}

export const fetchSuccess = (payload) => {
  return {
    type: "FETCH-SUCCESS",
    payload
  }
}

export const fetchFailure = (error) => {
  return {
    type: "FETCH-FAILURE",
    error
  }
}

export default {
  fetchStart,
  fetchSuccess,
  fetchFailure
}
