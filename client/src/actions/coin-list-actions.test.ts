
import fetchMock from 'fetch-mock'
import initializeMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import {
  fetchStart,
  fetchSuccess,
  fetchFailure,
  fetchCoinsList
} from "./coin-list-actions"

let coinListData = {
  coins: [{
    delta_24h: "2.81",
    market_cap: "3161162707237.9116210938",
    name: "Ethereum",
    price: "27980.953176572",
    rank: 2,
    show_symbol: "ETH",
    symbol: "ETH",
    volume_24h: "81212791781.3007202148",
  }, {
    delta_24h: "1.4",
    market_cap: "15581377069530.537109375",
    name: "Bitcoin",
    price: "841557.12058488",
    rank: 1,
    show_symbol: "BTC",
    symbol: "BTC",
    volume_24h: "170922970764.2334594727",
  }]
}

const middlewares = [thunk]
const mockStore = initializeMockStore(middlewares)

describe('coin list actions', () => {
  it('should create a fetch start action', () => {
    const expectedAction = {
      type: "FETCH-START"
    }
    expect(fetchStart()).toEqual(expectedAction)
  })

  it('should create a fetch success action', () => {
    let payload = [{ data: [{ name: "Bitcoin" }] }];
    const expectedAction = {
      type: "FETCH-SUCCESS",
      payload
    }
    expect(fetchSuccess(payload)).toEqual(expectedAction)
  })

  it('should create a fetch failure action', () => {
    let error = { error: 'failed' };
    const expectedAction = {
      type: "FETCH-FAILURE",
      error
    }
    expect(fetchFailure(error)).toEqual(expectedAction)
  });
});


describe('searchData', () => {
  let store;

  beforeEach(() => {
    store = mockStore({ coinList: [] })
  })

  afterEach(() => {
    fetchMock.restore()
  })

  it('Request without query params', () => {
    fetchMock.getOnce(`http://localhost:5000/getCoins`, {
      body: coinListData,
      headers: { 'content-type': 'application/json' }
    })

    return store.dispatch(fetchCoinsList("")).then(() => {
      let searchURL = fetchMock.lastUrl() as string
      let queryParams = searchURL.split('?')[1]
      expect(new URLSearchParams(queryParams).get('search')).toEqual(null);
    })
  });

  it('Request with query params', () => {
    fetchMock.getOnce(`http://localhost:5000/getCoins?search=bit`, {
      body: coinListData,
      headers: { 'content-type': 'application/json' }
    })

    return store.dispatch(fetchCoinsList("bit")).then(() => {
      let searchURL = fetchMock.lastUrl() as string
      let queryParams = searchURL.split('?')[1]
      expect(new URLSearchParams(queryParams).get('search')).toEqual('bit');
    })
  });

  it('Success flow', () => {
    fetchMock.getOnce(`http://localhost:5000/getCoins`, {
      body: coinListData,
      headers: { 'content-type': 'application/json' }
    })

    const expectedActions = [
      { type: "FETCH-START" },
      { type: "FETCH-SUCCESS", payload: coinListData.coins }
    ]

    return store.dispatch(fetchCoinsList("")).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('Failure flow', () => {
    const error = new Error('Fetch failed')
    fetchMock.mock(`http://localhost:5000/getCoins`, () => {
      throw error
    })

    const expectedActions = [
      { type: "FETCH-START" },
      { type: "FETCH-FAILURE", error }
    ]

    return store.dispatch(fetchCoinsList("")).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})
