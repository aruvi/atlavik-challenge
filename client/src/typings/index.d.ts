type Order = 'asc' | 'desc';

type sortType = 'asc' | 'desc';


interface coinData {
  name: string;
  rank: number;
  show_symbol: string;
  symbol: string;
  price: string;
  market_cap: string;
  volume_24h: string;
  delta_24h: string;
}

interface EnhancedTableProps {
  classes?: ReturnType<typeof useStyles>;
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof coinData) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof coinData;
  label: string;
  numeric: boolean;
}

interface EnhancedTableToolbarProps {
  searchCallback: Function;
  isLoading: Boolean;
}
