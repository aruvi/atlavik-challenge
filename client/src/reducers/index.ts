import coinList from "./coin-list-reducer"
import { combineReducers } from "redux"

export default combineReducers({
  coinList
})
