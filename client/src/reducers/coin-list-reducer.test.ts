import coinListReducer from "./coin-list-reducer"

const initialState = { isLoading: false, data: [], error: {} }

describe("CoinList reducer", () => {
  it("should return the initial state", () => {
    expect(coinListReducer(undefined, {})).toEqual(initialState)
  })

  it("should update isLoading in state", () => {
    expect(coinListReducer(initialState, { type: "FETCH-START" })).toEqual({ ...initialState, isLoading: true })
  })

  it("should update the coin list in state", () => {
    expect(coinListReducer(initialState, { type: "FETCH-SUCCESS", payload: [{ data: [{ name: "Bitcoin" }] }] })).toEqual({ ...initialState, isLoading: false, data: [{ data: [{ name: "Bitcoin" }] }] })
  })

  it("should update the error in state", () => {
    expect(coinListReducer(initialState, { type: "FETCH-FAILURE", payload: { error: "unknown error" } })).toEqual({ ...initialState, isLoading: false, error: { error: "unknown error" } })
  })
})
