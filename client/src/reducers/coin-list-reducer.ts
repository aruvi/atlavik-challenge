const coinListReducer = (state = { isLoading: false, data: [], error: {} }, action) => {
  switch (action.type) {
    case "FETCH-START":
      return { ...state, isLoading: true }
    case "FETCH-SUCCESS":
      return { ...state, data: action.payload, isLoading: false }
    case "FETCH-FAILURE":
      return { ...state, error: action.payload, isLoading: false }
    default:
      return state
  }
}

export default coinListReducer
