import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk';

const mockStore = configureMockStore([thunk]);

describe("App component", () => {
  it('should match the snapshot', () => {

    const store = mockStore({
      coinList: {
        data: [], isLoading: false, error: {}
      }
    });

    const wrapper = shallow(
      <Provider store={store}>
        <App />
      </Provider>
    )
    expect(wrapper.html()).toMatchSnapshot();
  });
})

