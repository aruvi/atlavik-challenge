import { createSelector } from "reselect"
import memoize from "lodash.memoize"

interface reduxState {
  coinList: coinListState
}

interface coinListState {
  data: coinData[]
}

interface sortConfig {
  sortType: sortType,
  sortKey: string
}

export const coinListSelector = (state: reduxState) => state.coinList

export const sortCoinListSelector = createSelector(
  coinListSelector,
  (coinList: coinListState) => memoize(
    (sortConfig: sortConfig) => coinList.data.sort((a: coinData, b: coinData) => {
      const { sortKey, sortType } = sortConfig
      const isDecendingSort = sortType === "desc"
      const signWrapper = sortKey === "name" || sortKey === "rank" ? +1 : -1
      const aValue = sortKey === "name" ? a[sortKey].toLowerCase() : parseFloat(a[sortKey])
      const bValue = sortKey === "name" ? b[sortKey].toLowerCase() : parseFloat(b[sortKey])

      if (aValue > bValue) {
        return signWrapper * (isDecendingSort ? -1 : 1)
      }

      if (aValue < bValue) {
        return signWrapper * (isDecendingSort ? 1 : -1)
      }

      return 0
    })
  )
)
