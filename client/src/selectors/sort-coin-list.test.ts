import { sortCoinListSelector, coinListSelector } from "./sort-coin-list"

let coinListData = {
  data: [{
    delta_24h: "2.81",
    market_cap: "3161162707237.9116210938",
    name: "Ethereum",
    price: "27980.953176572",
    rank: 2,
    show_symbol: "ETH",
    symbol: "ETH",
    volume_24h: "81212791781.3007202148",
  }, {
    delta_24h: "1.4",
    market_cap: "15581377069530.537109375",
    name: "Bitcoin",
    price: "841557.12058488",
    rank: 1,
    show_symbol: "BTC",
    symbol: "BTC",
    volume_24h: "170922970764.2334594727",
  }]
}

describe("sortCoinListSelector", () => {
  it("Sorts based on rank ascending", () => {
    const sortedData = sortCoinListSelector.resultFunc(coinListData)({ sortKey: "rank", sortType: "asc" })
    expect(sortedData[0].rank).toBe(1)
  })

  it("Sorts based on rank descending", () => {
    const { data } = coinListData
    const sortedData = sortCoinListSelector.resultFunc({ data: [data[1], data[0]] })({ sortKey: "rank", sortType: "desc" })
    expect(sortedData[0].rank).toBe(2)
  })

  it("Sorts based on name ascending", () => {
    const { data } = coinListData
    const sortedData = sortCoinListSelector.resultFunc(coinListData)({ sortKey: "name", sortType: "asc" })
    expect(sortedData[0].name).toBe("Bitcoin")
  })

  it("Sorts based on market cap descending", () => {
    const { data } = coinListData
    const sortedData = sortCoinListSelector.resultFunc(coinListData)({ sortKey: "market_cap", sortType: "desc" })
    expect(sortedData[0].name).toBe("Ethereum")
  })

  it("Selectes coin list data", () => {
    const listData = coinListSelector({ coinList: coinListData })
    expect(listData.data[0].name).toBe("Ethereum")
  })
})
