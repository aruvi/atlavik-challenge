# Atlavik Developer Challenge

I have used the [coinLib](https://coinlib.io/) API to fetch the details of the crypto currencies

To install all the dependencies(Client and Server) use npm-install.sh bash script

To start both Client and Server use start.sh bash script

## Goals achived

:heavy_check_mark: Record sorting
####
:heavy_check_mark: Record search (from backend)

## Memory efficiency

I have memonized the sorting functions in the client side with reselect selectors and in Server I am using worker threads to handle the search operation

## Testing
Server(Node JS App)
####
![GitHub Logo](images/server.png)

####

Client(React/Redux App)
####
![GitHub Logo](images/client.png)

I have covered most of the backend part in test cases, but in front end I wasnt able to write proper test cases the issue was related to material UI library, it was always throwing nothing was rendered error, I tired my best but I wasnt to able to find a solution.
