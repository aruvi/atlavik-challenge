import { Request, Response } from "express"
import { get } from "../utils/request"
import cache from "../utils/cache"
import spawnWorker from "../utils/spawnWorker"

export const getCryptoCurrentPriceList = async (req: Request, res: Response) => {
  const searchString: string = req.query.search as string
  const disableCache: Boolean = process.env.DISABLE_CACHE === "true" || false

  let priceList: PriceList | undefined = disableCache ? undefined : cache.get("cryptoPriceList")
  if (!priceList) {
    try {
      const API_KEY = process.env.COINLIB_API_KEY
      priceList = await get<PriceList>(`https://coinlib.io/api/v1/coinlist?key=${API_KEY}&pref=INR&order=rank_asc`)
      if (!disableCache) cache.set("cryptoPriceList", priceList)
    } catch (error) {
      res.status(500).end()
    }
  }

  if (!!searchString) {
    try {
      priceList = await spawnWorker({ priceList, searchString })
    } catch (error) {
      res.status(500).end()
    }
  }

  res.status(200).send(priceList)
}

