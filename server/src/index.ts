import express from 'express';
import cors from "cors"
import { getCryptoCurrentPriceList } from "./controller/cryptoCurrencyList"

const app: express.Application = express();
app.use(cors());

app.get('/getCoins', getCryptoCurrentPriceList);

if (!process.env.COINLIB_API_KEY) {
  console.error("Cannot find COINLIB_API_KEY environment variable")
  process.exit(1)
}

app.listen(5000, function () {
  console.log('Example app listening on port 5000!');
});

export default app
