interface CryptoCurrency {
  name: string;
}

interface PriceList {
  coins: CryptoCurrency[];
}

interface WorkerData {
  priceList: PriceList;
  searchString: string;
}
