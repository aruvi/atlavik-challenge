// Import the dependencies for testing
import chai, { expect } from 'chai'
import chaiHttp from 'chai-http'
import sinon from 'sinon'
import cache from './utils/cache'
import * as request from './utils/request'
import app from './index'
import * as spawnWorker from "./utils/spawnWorker"

chai.use(chaiHttp)
chai.should()

describe("cryptoCurrencyList", () => {
  describe("GET /coins", () => {

    let sandbox: sinon.SinonSandbox

    beforeEach(function () {
      sandbox = sinon.createSandbox()
    })

    afterEach(function () {
      sandbox.resetBehavior()
      sandbox.resetHistory()
    })

    it("Should return the data from the cache", (done) => {
      const cacheData = { coins: [{ name: "bitcoin" }] }
      sandbox.stub(cache, "get").returns(cacheData)
      chai.request(app)
        .get('/getCoins')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          expect(res.body).to.deep.equal(cacheData)
          done()
        })
    })

    it("Should fetch the data from public api", (done) => {
      const publicApiResponse = { coins: [{ name: "ethereum" }] }
      sandbox.stub(request, "get").resolves(publicApiResponse)
      chai.request(app)
        .get('/getCoins')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          expect(res.body).to.deep.equal(publicApiResponse)
          sandbox.restore()
          done()
        })
    })

    it("Fetch the data from public api will fail", (done) => {
      const publicApiErrorResponse = { error: "Unexpected Error" }
      sandbox.stub(request, "get").rejects(publicApiErrorResponse)
      chai.request(app)
        .get('/getCoins')
        .end((err, res) => {
          res.should.have.status(500)
          sandbox.restore()
          done()
        })
    })

    it("Filters data based on the search query", (done) => {
      const searchResult = { coins: [{ name: "bitcoin Lite" }] }
      sandbox.stub(spawnWorker, "default").resolves(searchResult)
      sandbox.stub(request, "get").resolves(searchResult)
      chai.request(app)
        .get('/getCoins?search=bitcoin')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          expect(res.body).to.deep.equal(searchResult)
          sandbox.restore()
          done()
        })
    })

    it("Filters data based on the search query fail", (done) => {
      const searchFailed = { error: "Unexpected Error" }
      sandbox.stub(spawnWorker, "default").rejects(searchFailed)
      chai.request(app)
        .get('/getCoins?search=bitcoin')
        .end((err, res) => {
          res.should.have.status(500)
          sandbox.restore()
          done()
        })
    })
  })
})
