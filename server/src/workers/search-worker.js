const { workerData, parentPort } = require("worker_threads")

const { searchString, priceList } = workerData

const searchResult = priceList.coins.filter((crypto) => {
  return crypto.name.toLowerCase().includes(searchString)
})

parentPort.postMessage({ coins: searchResult });
