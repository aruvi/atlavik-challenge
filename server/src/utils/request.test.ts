import { expect } from "chai"
import { get } from "./request"

describe("Request", () => {
  it("returns a promise", () => {
    expect(get("https://google.com")).be.a('promise')
  })
})
