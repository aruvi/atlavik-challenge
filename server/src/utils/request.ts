import { IncomingMessage } from "http"
import https from "https"

export const get = <T>(url: string): Promise<T> => {
  return new Promise((resolve, reject) => {
    const req = https.get(url, (res: IncomingMessage) => {
      if (res.statusCode < 200 || res.statusCode >= 300) {
        console.log("error")
        return reject(new Error(`statusCode=${res.statusCode}`))
      }

      let body: Uint8Array[] = []
      res.on("data", (chunk: Uint8Array) => {
        body.push(chunk)
      })

      res.on("end", () => {
        try {
          const jsonBody: T = JSON.parse(Buffer.concat(body).toString())
          resolve(jsonBody)
        } catch (e) {
          console.log("error", e)
          reject(e)
        }
      })
    })

    req.on("error", (err: Error) => {
      console.log("error", err)
      reject(err)
    })

    req.end()
  })
}
