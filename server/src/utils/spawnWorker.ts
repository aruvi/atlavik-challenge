import { Worker } from "worker_threads";

interface CryptoCurrency {
  name: string;
}

interface PriceList {
  coins: CryptoCurrency[];
}

interface WorkerData {
  priceList: PriceList;
  searchString: string;
}

export default (workerData: WorkerData) => {
  return new Promise<PriceList>((resolve, reject) => {
    const worker = new Worker("./src/workers/search-worker.js", { workerData });
    worker.on("message", resolve);
    worker.on("error", reject);
    worker.on("exit", code => code !== 0 && reject(new Error(`Worker stopped with 
        exit code ${code}`)));
  })
}
